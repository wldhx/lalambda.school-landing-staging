#!/bin/sh
pandoc ru.yml -f markdown \
	--template index.md \
	| pandoc - ru.yml -f markdown --template template.pandoc --css index.css -s -o public/index.html
pandoc en.yml -f markdown \
	--template en.md \
	| pandoc - en.yml -f markdown --template template.pandoc --css index.css -s -o public/en.html
