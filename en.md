<div class="container">

<header class="header">

<div id="title"><h1>$title1$</h1></div>

<div id="subtitle"><h2>$subtitle1$</h2></div>

<div id="year"><h2>'21</h2></div>

<h3 id="subsubtitle">$subsubtitle1$</h3>

</header>

<section id="hero">

<div id="points">$points$</div>
<div id="date">17/07-01/08</div>
<div id="news">[$news$→](https://t.me/lalambdaschool)</div>

</section>

$programme$

$programmeoneday$

$participants$

$venue$

<div style="position:relative;overflow:hidden;"><a href="https://yandex.ru/maps?utm_medium=mapframe&utm_source=maps" style="color:#eee;font-size:12px;position:absolute;top:0px;">Яндекс.Карты</a><a href="https://yandex.ru/maps/?ll=37.230457%2C55.774834&mode=routes&rtext=55.751812%2C37.599292~55.764818%2C36.868428&rtt=auto&ruri=ymapsbm1%3A%2F%2Fgeo%3Fll%3D37.599%252C55.752%26spn%3D0.001%252C0.001%26text%3D%25D0%25A0%25D0%25BE%25D1%2581%25D1%2581%25D0%25B8%25D1%258F%252C%2520%25D0%259C%25D0%25BE%25D1%2581%25D0%25BA%25D0%25B2%25D0%25B0%252C%2520%25D1%2583%25D0%25BB%25D0%25B8%25D1%2586%25D0%25B0%2520%25D0%2590%25D1%2580%25D0%25B1%25D0%25B0%25D1%2582%252C%25201~ymapsbm1%3A%2F%2Forg%3Foid%3D1126428022&utm_medium=mapframe&utm_source=maps&z=10" style="color:#eee;font-size:12px;position:absolute;top:14px;">Яндекс.Карты</a><iframe src="https://yandex.ru/map-widget/v1/-/CCUYmFBDlB" width="560" height="400" frameborder="1" allowfullscreen="true" style="position:relative;"></iframe></div>

$questions$

$cta$

<footer>
<span id="html5">
pandoc/HTML5
</span>
|
<span>
[designed](https://gitlab.com/wldhx/lalambda.school-landing) by 🐓 engineers with ❤️
</span>
</footer>

</div>
